﻿'
' Created by SharpDevelop.
' User: ta185044
' Date: 6/25/2012
' Time: 1:14 PM
' 
' To change this template use Tools | Options | Coding | Edit Standard Headers.
'
Imports Microsoft.Win32
'Imports System
Public Partial Class AboutTheTool
	Public Sub New()
		' The Me.InitializeComponent call is required for Windows Forms designer support.
		
			
			Me.InitializeComponent()
			'lblDesc.Text = "The purpose of the tool is to provide key performance measures of deployed SSCO system without affected by shopper interactions. Which allows assessment and comparison of the software and system performance between releases and configurations and serve to investigate claims of poor performance by store personnel." 
			GetToolInformation()
		'
		' TODO : Add constructor code after InitializeComponents
		'
	End Sub
	
	Sub BtnCloseAboutClick(sender As Object, e As EventArgs)
		Me.Close()		
	End Sub
	
	Private Sub GetToolInformation()
        Const userRoot As String = "HKEY_LOCAL_MACHINE"
        Const subkey As String = "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\SSCO Application Performance Assessment Tool"
        Const keyName As String = userRoot & "\" & subkey
        Dim versionNum As String
        'Dim OSVersion As String
        'Dim NETVersion As String
        'Dim otherInfo As String
        Try
        	versionNum = Registry.GetValue(keyName,"DisplayVersion","1.0.0")
        	lblVersion.Text = versionNum
        	'NETVersion = Environment.Version.ToString()
        	'OSVersion = Environment.OSVersion.ToString()
        	'otherInfo = "More Information: " & vbNewLine & vbNewLine & ".NET Version      :  " & NETVersion & vbNewLine & "OS Version        :  " & OSVersion
        	'moreInforBox.Text = otherInfo 
        	
        'Registry.SetValue(keyName, "Version", sVERSION)
        'Registry.SetValue(keyName, "DictionaryDir", g_sDICTIONARY_DIR)
        'Registry.SetValue(keyName, "InitialDir", g_sINITIAL_DIRECTORY)
        Catch ex As Exception
        	MessageService.ShowWarning(ex.ToString)
        End Try
        	
    End Sub
	
End Class
